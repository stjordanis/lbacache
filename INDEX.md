# LBACACHE

Disk cache, caches reads for max 8 CHS / LBA hard disks and floppies, XMS, 386 or better - tickle comes with lbacache!


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## LBACACHE.LSM

<table>
<tr><td>title</td><td>LBACACHE</td></tr>
<tr><td>version</td><td>2008apr07 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2008-07-23</td></tr>
<tr><td>description</td><td>disk cache for hard drives and floppy diskettes</td></tr>
<tr><td>summary</td><td>Disk cache, caches reads for max 8 CHS / LBA hard disks and floppies, XMS, 386 or better - tickle comes with lbacache!</td></tr>
<tr><td>keywords</td><td>cache, smartdrv, nwcache</td></tr>
<tr><td>author</td><td>Eric Auer &lt;eric -at- coli.uni-sb.de&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer &lt;eric -at- coli.uni-sb.de&gt;</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/lbacache/</td></tr>
<tr><td>platforms</td><td>DOS 386+ (nasm assembler), FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
